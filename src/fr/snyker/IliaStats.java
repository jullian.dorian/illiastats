package fr.snyker;

import org.bukkit.plugin.java.JavaPlugin;

import fr.snyker.command.IliaCommand;
import fr.snyker.events.EventsManager;
import fr.snyker.management.ConfigManager;

public class IliaStats extends JavaPlugin{
	
	private static IliaStats instance;

	@Override
	public void onEnable() {
		instance = this;
		new EventsManager(this).registerListener();
		ConfigManager.getInstance().setup(this);
		
		saveDefaultConfig();
		
		//Command
		getCommand("istats").setExecutor(new IliaCommand());
	}
	
	@Override
	public void onDisable() {
		ConfigManager.getInstance().saveP();
		saveConfig();
	}
	
	public static IliaStats iStats(){
		return instance;
	}

}
