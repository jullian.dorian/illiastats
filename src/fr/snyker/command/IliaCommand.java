package fr.snyker.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.snyker.IliaStats;
import fr.snyker.inventaire.Panel;
import fr.snyker.management.ConfigManager;
import fr.snyker.utils.UtilsCommand;
import fr.snyker.utils.command.Help;
import fr.snyker.utils.command.SeePlayer;
import fr.snyker.utils.players.Create;
import fr.snyker.utils.players.Remove;

public class IliaCommand implements CommandExecutor {
	
	ConfigManager user = ConfigManager.getInstance();
	IliaStats config = IliaStats.iStats();
	
	@Override
	public boolean onCommand(CommandSender send, Command cmd, String label, String[] args) {
		Player player = (Player) send;
		String pseudo = player.getName();
		
		if(args.length != 0){
			//COMMANDE CREATE POUR ETRE ENREGISTRE DANS LA CONFIG
			if(args[0].equalsIgnoreCase("create") && player.hasPermission("istats.create")){
				Create.createPlayer(player, pseudo);
			}
			
			//COMMANDE REMOVE SUPPRIMER UN JOUEUR 
			else if(args[0].equalsIgnoreCase("remove")){
				Remove.removePlayer(player, args);
			}
			
			//COMMANDE VOIR UN JOUEUR 
			else if(args[0].equalsIgnoreCase("see") && player.hasPermission("istats.see.player")){
				if(args.length == 2){
					SeePlayer.seePlayer(player, Bukkit.getPlayerExact(args[1]));
				} else {
					player.sendMessage("§cErreur: Vous devez précisez un joueur !");
					player.sendMessage("§f/stats see §c<player>");
				}
			}
			
			//COMMANDE POUR LE LIVRE
			else if(args[0].equalsIgnoreCase("book")){
				if(args.length == 2){
					if(args[1].equalsIgnoreCase("give")){
						UtilsCommand.giveBook(player);
					} else if(args[1].equalsIgnoreCase("remove")){
						UtilsCommand.removeBook(player);
					}
				} else {
					player.sendMessage("§cErreur: Vous devez précisez un argument !");
				}
				
			}
			
			//COMMANDE AFFICHE LE MENU
			else if(args[0].equalsIgnoreCase("menu") && player.hasPermission("istats.menu")){
				if(user.getP().contains(player.getUniqueId().toString())){
					Panel.openStats(player);
				} else {
					player.sendMessage("§cErreur: Vous n'êtes pas enregistrés dans nos fichiers de configuration.");
					player.sendMessage("§eMerci de déconnecter/reconnecter ou de §acrée un compte : §f/stats create");
				}
			}
			
			//COMMANDE RELOAD DES CONFIGS
			else if(args[0].equalsIgnoreCase("reload") && player.hasPermission("istats.reload") || player.isOp() == true){
				this.config.reloadConfig();
				this.user.reloadP();
				player.sendMessage("§c[§7IliaStats§c] §aLe plugin à redémarrer ses fichiers de configuration.");
			} 
			
			//Pour toutes les permissions null
			else {
				player.sendMessage("§cErreur: Vous ne possedez la permission pour éxécuter cette commande ou alors la commande à été mal tapez.");
				player.sendMessage("§eMerci de regardez les commandes disponibles.");
				return false;
			}
		
		//Si l'argument ne dépasse pas " /stats " on n'evoie la liste des commandes.
		} else {
			//COMMANDE HELP
			Help.commandHelp(player);
		}
		return false;
	}

}
/**
#- stats.help                            
#- stats.see.player                       
#- stats.remove.me                           
#- stats.remove.player                    
#- stats.create                           
#- stats.create.onjoin                     
#- stats.reload    
#- stats.menu.inventory.open               
#- stats.book.give                    
#- stats.book.remove
stats.book.receive.onjoin    
#- stats.book 
*/
