package fr.snyker.events;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import fr.snyker.events.inventaire.ActionPanel;
import fr.snyker.events.inventaire.stats.ActionAgilite;
import fr.snyker.events.inventaire.stats.ActionForce;
import fr.snyker.events.inventaire.stats.ActionResistance;
import fr.snyker.events.inventaire.stats.ActionVitalite;
import fr.snyker.events.inventaire.stats.ActionVitesse;
import fr.snyker.events.player.ActionInventory;
import fr.snyker.events.player.ActionPlayer;
import fr.snyker.events.player.DropItem;
import fr.snyker.events.player.PlayerJoin;
import fr.snyker.events.player.PlayerVersusPlayer;
import fr.snyker.events.player.RespawnPlayer;
import fr.snyker.events.player.stats.Force;
import fr.snyker.events.player.stats.Resistance;
import fr.snyker.events.player.stats.Vitalite;

public class EventsManager {

	Plugin pl;
	
	public EventsManager(Plugin pl){
		this.pl = pl;
	}
	
	public void registerListener(){
		PluginManager pm = Bukkit.getPluginManager();
		
		//Event principaux
		pm.registerEvents(new PlayerJoin(), pl);
		pm.registerEvents(new ActionPlayer(), pl);
		pm.registerEvents(new ActionInventory(), pl);
		pm.registerEvents(new RespawnPlayer(), pl);
		pm.registerEvents(new DropItem(), pl);
		pm.registerEvents(new PlayerVersusPlayer(), pl);
			//Events Stats
			pm.registerEvents(new Force(), pl);
			pm.registerEvents(new Resistance(), pl);
			pm.registerEvents(new Vitalite(), pl);
		
		//Event Inventaire
			//Event panel
			pm.registerEvents(new ActionPanel(), pl);
				//Event stats
				pm.registerEvents(new ActionAgilite(), pl);
				pm.registerEvents(new ActionVitesse(), pl);
				pm.registerEvents(new ActionForce(), pl);
				pm.registerEvents(new ActionResistance(), pl);
				pm.registerEvents(new ActionVitalite(), pl);
	}
	
}
