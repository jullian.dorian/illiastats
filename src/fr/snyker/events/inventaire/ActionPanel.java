package fr.snyker.events.inventaire;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.snyker.inventaire.Panel;
import fr.snyker.inventaire.stats.PalierAgilite;
import fr.snyker.inventaire.stats.PalierForce;
import fr.snyker.inventaire.stats.PalierResistance;
import fr.snyker.inventaire.stats.PalierVitalite;
import fr.snyker.inventaire.stats.PalierVitesse;
import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;
import fr.snyker.utils.Threads;

public class ActionPanel implements Listener{
	
	ConfigManager user = ConfigManager.getInstance();
	
	@EventHandler
    public void onClickInventaire(InventoryClickEvent e){
    	Player p = (Player) e.getWhoClicked();
		
    	//Ont look le nom de l'inventaire
    	if(e.getInventory().getName().equals(StringName.namePanel(p))){
    		//SI le joueur clique dans du vide ou sur une case vide ont return
    		if(e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR){ return; }
    		//Ont empeche de cliquer sur les items
    		e.setCancelled(true);
    		
    		//Si l'item n'est pas nul est correspond au bon nom
    		if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()){
				switch(e.getCurrentItem().getItemMeta().getDisplayName()){

				case "§eAméliorez votre compétence en §a\u273f Agilite":
					if(e.getClick() == ClickType.MIDDLE){
						if(Threads.getPoints(p) >= 0){
							p.openInventory(PalierAgilite.panelAgilite(p));
						}
					} else
						if(e.getClick() == ClickType.RIGHT){
							if(Threads.getPoints(p) >= 5){
								Threads.removePoints(p, 5);
								Threads.addPointAgi(p, 5);
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 5 au minimum.");
								break;
							}
					} else
						if(e.getClick() == ClickType.LEFT){
							if(Threads.getPoints(p) >= 1){
								Threads.removePoints(p, 1);
								Threads.addPointAgi(p, 1);
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 1 au minimum.");
								break;
							}
					} else 
						if(e.getClick() == ClickType.SHIFT_LEFT){
							if(Threads.getPoints(p) >= 1){
								Threads.addPointAgi(p, Threads.getPoints(p));
								Threads.removePoints(p, Threads.getPoints(p));
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 1 au minimum.");
							}
					}
					break;
					
				case "§eAméliorez votre compétence en §b\u2747 Vitesse":
					if(e.getClick() == ClickType.MIDDLE){
						if(Threads.getPoints(p) >= 0){
							p.openInventory(PalierVitesse.panelVitesse(p));
						}
					} else
						if(e.getClick() == ClickType.RIGHT){
							if(Threads.getPoints(p) >= 5){
								Threads.removePoints(p, 5);
								Threads.addPointVit(p, 5);
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 5 au minimum.");
								break;
							}
					} else
						if(e.getClick() == ClickType.LEFT){
							if(Threads.getPoints(p) >= 1){
								Threads.removePoints(p, 1);
								Threads.addPointVit(p, 1);
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 1 au minimum.");
								break;
							}
					} else 
						if(e.getClick() == ClickType.SHIFT_LEFT){
							if(Threads.getPoints(p) >= 1){
								Threads.addPointVit(p, Threads.getPoints(p));
								Threads.removePoints(p, Threads.getPoints(p));
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 1 au minimum.");
							}
					}
					break;
					
				case "§eAméliorez votre compétence en §c\u2734 Force":
					if(e.getClick() == ClickType.MIDDLE){
						if(Threads.getPoints(p) >= 0){
							p.openInventory(PalierForce.panelForce(p));
						}
					} else
						if(e.getClick() == ClickType.RIGHT){
							if(Threads.getPoints(p) >= 5){
								Threads.removePoints(p, 5);
								Threads.addPointFor(p, 5);
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 5 au minimum.");
								break;
							}
					} else
						if(e.getClick() == ClickType.LEFT){
							if(Threads.getPoints(p) >= 1){
								Threads.removePoints(p, 1);
								Threads.addPointFor(p, 1);
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 1 au minimum.");
								break;
							}
					} else 
						if(e.getClick() == ClickType.SHIFT_LEFT){
							if(Threads.getPoints(p) >= 1){
								Threads.addPointFor(p, Threads.getPoints(p));
								Threads.removePoints(p, Threads.getPoints(p));
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 1 au minimum.");
							}
					}
					break;
					
				case "§eAméliorez votre compétence en §8\u2b22 Resistance":
					if(e.getClick() == ClickType.MIDDLE){
						if(Threads.getPoints(p) >= 0){
							p.openInventory(PalierResistance.panelResistance(p));
						}
					} else
						if(e.getClick() == ClickType.RIGHT){
							if(Threads.getPoints(p) >= 5){
								Threads.removePoints(p, 5);
								Threads.addPointRes(p, 5);
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 5 au minimum.");
								break;
							}
					} else
						if(e.getClick() == ClickType.LEFT){
							if(Threads.getPoints(p) >= 1){
								Threads.removePoints(p, 1);
								Threads.addPointRes(p, 1);
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 1 au minimum.");
								break;
							}
					} else 
						if(e.getClick() == ClickType.SHIFT_LEFT){
							if(Threads.getPoints(p) >= 1){
								Threads.addPointRes(p, Threads.getPoints(p));
								Threads.removePoints(p, Threads.getPoints(p));
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 1 au minimum.");
							}
					}
					break;
					
				case "§eAméliorez votre compétence en §6\u2764 Vitalite":
					if(e.getClick() == ClickType.MIDDLE){
						if(Threads.getPoints(p) >= 0){
							p.openInventory(PalierVitalite.panelVitalite(p));
						}
					} else
						if(e.getClick() == ClickType.RIGHT){
							if(Threads.getPoints(p) >= 5){
								Threads.removePoints(p, 5);
								Threads.addPointVita(p, 5);
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 5 au minimum.");
								break;
							}
					} else
						if(e.getClick() == ClickType.LEFT){
							if(Threads.getPoints(p) >= 1){
								Threads.removePoints(p, 1);
								Threads.addPointVita(p, 1);
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 1 au minimum.");
								break;
							}
					} else 
						if(e.getClick() == ClickType.SHIFT_LEFT){
							if(Threads.getPoints(p) >= 1){
								Threads.addPointVita(p, Threads.getPoints(p));
								Threads.removePoints(p, Threads.getPoints(p));
								Panel.openStats(p);
							} else {
								p.sendMessage("§cVous ne possèdez pas assez de points !");
								p.sendMessage("§cIl vous en faut 1 au minimum.");
							}
					}
					break;
					
				case "§cFermer le Panel":
					p.closeInventory();
					break;
				
				default:
					break; 
				}
    		}
    	}
    }

}
