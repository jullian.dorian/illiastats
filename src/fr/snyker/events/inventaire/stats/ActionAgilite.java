package fr.snyker.events.inventaire.stats;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.snyker.inventaire.Panel;
import fr.snyker.utils.StringName;

public class ActionAgilite implements Listener{
	
	@EventHandler
	public void actionVitesse(InventoryClickEvent event){
		
		Player p =  (Player) event.getWhoClicked();
		
		//Ont look le nom de l'inventaire
    	if(event.getInventory().getName().equals(StringName.panelAgilite)){
    		//SI le joueur clique dans du vide ou sur une case vide ont return
    		if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR){ return; }
    		//Ont empeche de cliquer sur les items
    		event.setCancelled(true);
    		
    		//Si l'item n'est pas nul est correspond au bon nom
    		if(event.getCurrentItem() != null && event.getCurrentItem().hasItemMeta()){
				switch(event.getCurrentItem().getItemMeta().getDisplayName()){
				
				case "§bJump x1.5":
					if(p.hasPotionEffect(PotionEffectType.JUMP)){
						p.removePotionEffect(PotionEffectType.JUMP);
						p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2555555, 0));
						p.sendMessage("§aVous sautez plus haut dorénavant !");
						p.sendMessage("§4/ATTENTION\\ §cSi vous mourez l'effet disparaitra ! Il faudras donc le ré-activer.");
					} else {
						p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2555555, 0));
						p.sendMessage("§aVous sautez plus haut dorénavant !");
						p.sendMessage("§4/ATTENTION\\ §cSi vous mourez l'effet disparaitra ! Il faudras donc le ré-activer.");
						p.closeInventory();
					}
					break;
					
				case "§bJump x2":
					if(p.hasPotionEffect(PotionEffectType.JUMP)){
						p.removePotionEffect(PotionEffectType.JUMP);
						p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2555555, 1));
						p.sendMessage("§aVous sautez plus haut dorénavant !");
						p.sendMessage("§4/ATTENTION\\ §cSi vous mourez l'effet disparaitra ! Il faudras donc le ré-activer.");
					} else {
						p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2555555, 1));
						p.sendMessage("§aVous sautez plus haut dorénavant !");
						p.sendMessage("§4/ATTENTION\\ §cSi vous mourez l'effet disparaitra ! Il faudras donc le ré-activer.");
						p.closeInventory();
					}
					break;
					
				case "§bJump x3":
					if(p.hasPotionEffect(PotionEffectType.JUMP)){
						p.removePotionEffect(PotionEffectType.JUMP);
						p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2555555, 2));
						p.sendMessage("§aVous sautez plus haut dorénavant !");
						p.sendMessage("§4/ATTENTION\\ §cSi vous mourez l'effet disparaitra ! Il faudras donc le ré-activer.");
					} else {
						p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2555555, 2));
						p.sendMessage("§aVous sautez plus haut dorénavant !");
						p.sendMessage("§4/ATTENTION\\ §cSi vous mourez l'effet disparaitra ! Il faudras donc le ré-activer.");
						p.closeInventory();
					}
					break;
					
				case "§bJump x4":
					if(p.hasPotionEffect(PotionEffectType.JUMP)){
						p.removePotionEffect(PotionEffectType.JUMP);
						p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2555555, 3));
						p.sendMessage("§aVous sautez plus haut dorénavant !");
						p.sendMessage("§4/ATTENTION\\ §cSi vous mourez l'effet disparaitra ! Il faudras donc le ré-activer.");
					} else {
						p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2555555, 3));
						p.sendMessage("§aVous sautez plus haut dorénavant !");
						p.sendMessage("§4/ATTENTION\\ §cSi vous mourez l'effet disparaitra ! Il faudras donc le ré-activer.");
						p.closeInventory();
					}
					break;
					
				case "§cSupprimer le bonus":
					p.removePotionEffect(PotionEffectType.JUMP);
					p.sendMessage("§cVous perdez l'effet : §a\"Agilité\" §cVotre Agilité repasse à son état normal.");
					p.closeInventory();
					break;
					
				case "§7Retourner au panel":
					Panel.openStats(p);
					break;
					
				default:
					break;
				}
    		}
    	}
		
	}
}
