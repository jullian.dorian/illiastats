package fr.snyker.events.inventaire.stats;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.snyker.inventaire.Panel;
import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;

public class ActionForce implements Listener{
	
	ConfigManager user = ConfigManager.getInstance();
	
	@EventHandler
	public void actionVitesse(InventoryClickEvent event){
		
		Player p =  (Player) event.getWhoClicked();
		String path = p.getUniqueId().toString()+".infos.stats.Force";
		String path1 = p.getUniqueId().toString()+".infos.stats.Force.level-1";
		String path2 = p.getUniqueId().toString()+".infos.stats.Force.level-2";
		String path3 = p.getUniqueId().toString()+".infos.stats.Force.level-3";
		String path4 = p.getUniqueId().toString()+".infos.stats.Force.level-4";
		
		//Ont look le nom de l'inventaire
    	if(event.getInventory().getName().equals(StringName.panelForce)){
    		//SI le joueur clique dans du vide ou sur une case vide ont return
    		if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR){ return; }
    		//Ont empeche de cliquer sur les items
    		event.setCancelled(true);
    		
    		//Si l'item n'est pas nul est correspond au bon nom
    		if(event.getCurrentItem() != null && event.getCurrentItem().hasItemMeta()){
				switch(event.getCurrentItem().getItemMeta().getDisplayName()){

				case "§5Dégâts +0.5":
					if(user.getP().getBoolean(path1) == true){
						user.getP().set(path1, false);
						user.saveP();
						p.sendMessage("§cVous avez désactiver l'effet. Vos dommages repasse à leur état initiaux.");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-1", true);
						user.getP().set(path2, false);
						user.getP().set(path3, false);
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§aVos dommages augmentes de +0.5 | [Arc non compris]");
						p.closeInventory();
					}
					break;
					
				case "§5Dégâts +1":
					if(user.getP().getBoolean(path2) == true){
						user.getP().set(path2, false);
						user.saveP();
						p.sendMessage("§cVous avez désactiver l'effet. Vos dommages repasse à leur état initiaux.");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-2", true);
						user.getP().set(path1, false);
						user.getP().set(path3, false);
						user.getP().set(path4, false);
						user.saveP();
						p.closeInventory();
						p.sendMessage("§aVos dommages augmentes de +1 | [Arc non compris]");
					}
					break;
					
				case "§5Dégâts +2":
					if(user.getP().getBoolean(path3) == true){
						user.getP().set(path3, false);
						user.saveP();
						p.sendMessage("§cVous avez désactiver l'effet. Vos dommages repasse à leur état initiaux.");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-3", true);
						user.getP().set(path1, false);
						user.getP().set(path2, false);
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§aVos dommages augmentes de +2 | [Arc non compris]");
						p.closeInventory();
					}
					break;
					
				case "§5Dégâts +4":
					if(user.getP().getBoolean(path4) == true){
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§cVous avez désactiver l'effet. Vos dommages repasse à leur état initiaux.");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-4", true);
						user.getP().set(path1, false);
						user.getP().set(path2, false);
						user.getP().set(path3, false);
						user.saveP();
						p.sendMessage("§aVos dommages augmentes de +4 | [Arc non compris]");
						p.closeInventory();
					}
					break;
					
				case "§cSupprimer le bonus":
					user.getP().set(path+".level-1", false);
					user.getP().set(path+".level-2", false);
					user.getP().set(path+".level-3", false);
					user.getP().set(path+".level-4", false);
					user.saveP();
					p.sendMessage("§cVous perdez l'effet : §7\"Force\" §cVotre Force repasse à son état normal.");
					p.closeInventory();
					break;
					
				case "§7Retourner au panel":
					Panel.openStats(p);
					break;
					
				default:
					break;
				}
    		}
    	}
		
	}

}
