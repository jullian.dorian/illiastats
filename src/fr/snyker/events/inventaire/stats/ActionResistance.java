package fr.snyker.events.inventaire.stats;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.snyker.inventaire.Panel;
import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;

public class ActionResistance implements Listener{
	
	ConfigManager user = ConfigManager.getInstance();
	
	@EventHandler
	public void actionVitesse(InventoryClickEvent event){
		
		Player p =  (Player) event.getWhoClicked();
		String path = p.getUniqueId().toString()+".infos.stats.Resistance";
		String path1 = p.getUniqueId().toString()+".infos.stats.Resistance.level-1";
		String path2 = p.getUniqueId().toString()+".infos.stats.Resistance.level-2";
		String path3 = p.getUniqueId().toString()+".infos.stats.Resistance.level-3";
		String path4 = p.getUniqueId().toString()+".infos.stats.Resistance.level-4";
		
		//Ont look le nom de l'inventaire
    	if(event.getInventory().getName().equals(StringName.panelResistance)){
    		//SI le joueur clique dans du vide ou sur une case vide ont return
    		if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR){ return; }
    		//Ont empeche de cliquer sur les items
    		event.setCancelled(true);
    		
    		//Si l'item n'est pas nul est correspond au bon nom
    		if(event.getCurrentItem() != null && event.getCurrentItem().hasItemMeta()){
				switch(event.getCurrentItem().getItemMeta().getDisplayName()){

				case "§8Réduire de -0.5":
					if(user.getP().getBoolean(path1) == true){
						user.getP().set(path1, false);
						user.saveP();
						p.sendMessage("§cVous perdez l'effet : §7\"Résistance\"");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-1", true);
						user.getP().set(path2, false);
						user.getP().set(path3, false);
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§aVous réduisez les dommages subits de -0.5");
						p.closeInventory();
					}
					break;
					
				case "§8Réduire de -1":
					if(user.getP().getBoolean(path2) == true){
						user.getP().set(path2, false);
						user.saveP();
						p.sendMessage("§cVous perdez l'effet : §7\"Résistance\"");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-2", true);
						user.getP().set(path1, false);
						user.getP().set(path3, false);
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§aVous réduisez les dommages subits de -1");
						p.closeInventory();
					}
					break;
					
				case "§8Réduire de -2":
					if(user.getP().getBoolean(path3) == true){
						user.getP().set(path3, false);
						user.saveP();
						p.sendMessage("§cVous perdez l'effet : §7\"Résistance\"");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-3", true);
						user.getP().set(path1, false);
						user.getP().set(path2, false);
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§aVous réduisez les dommages subits de -2");
						p.closeInventory();
					}
					break;
					
				case "§8Réduire de -4":
					if(user.getP().getBoolean(path4) == true){
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§cVous perdez l'effet : §7\"Résistance\"");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-4", true);
						user.getP().set(path1, false);
						user.getP().set(path2, false);
						user.getP().set(path3, false);
						user.saveP();
						p.sendMessage("§aVous réduisez les dommages subits de -4");
						p.closeInventory();
					}
					break;
					
				case "§cSupprimer le bonus":
					user.getP().set(path+".level-1", false);
					user.getP().set(path+".level-2", false);
					user.getP().set(path+".level-3", false);
					user.getP().set(path+".level-4", false);
					user.saveP();
					p.sendMessage("§cVous perdez l'effet : §7\"Résistance\" §cVotre Résistance repasse à son état normal.");
					break;
					
				case "§7Retourner au panel":
					Panel.openStats(p);
					break;
					
				default:
					break;
				}
    		}
    	}
		
	}

}
