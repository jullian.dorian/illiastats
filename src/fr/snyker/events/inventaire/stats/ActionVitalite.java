package fr.snyker.events.inventaire.stats;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.snyker.inventaire.Panel;
import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;

public class ActionVitalite implements Listener{
	
	ConfigManager user = ConfigManager.getInstance();
	
	@EventHandler
	public void actionVitesse(InventoryClickEvent event){
		
		Player p =  (Player) event.getWhoClicked();
		String path = p.getUniqueId().toString()+".infos.stats.Vitalite";
		String path1 = p.getUniqueId().toString()+".infos.stats.Vitalite.level-1";
		String path2 = p.getUniqueId().toString()+".infos.stats.Vitalite.level-2";
		String path3 = p.getUniqueId().toString()+".infos.stats.Vitalite.level-3";
		String path4 = p.getUniqueId().toString()+".infos.stats.Vitalite.level-4";
		
		//Ont look le nom de l'inventaire
    	if(event.getInventory().getName().equals(StringName.panelVitalite)){
    		//SI le joueur clique dans du vide ou sur une case vide ont return
    		if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR){ return; }
    		//Ont empeche de cliquer sur les items
    		event.setCancelled(true);
    		
    		//Si l'item n'est pas nul est correspond au bon nom
    		if(event.getCurrentItem() != null && event.getCurrentItem().hasItemMeta()){
				switch(event.getCurrentItem().getItemMeta().getDisplayName()){

				case "§eRégénération x50%":
					if(user.getP().getBoolean(path1) == true){
						user.getP().set(path1, false);
						user.saveP();
						p.sendMessage("§eVous perdez l'effet \"Régénération\"");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-1", true);
						user.getP().set(path2, false);
						user.getP().set(path3, false);
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§aVous regenerez 50% plus vite à partir de maintenant.");
						p.closeInventory();
					}
					break;
					
				case "§eRégénération x100%":
					if(user.getP().getBoolean(path2) == true){
						user.getP().set(path2, false);
						user.saveP();
						p.sendMessage("§eVous perdez l'effet \"Régénération\"");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-2", true);
						user.getP().set(path1, false);
						user.getP().set(path3, false);
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§aVous regenerez 100% plus vite à partir de maintenant.");
						p.closeInventory();
					}
					break;
					
				case "§eRégénération x150%":
					if(user.getP().getBoolean(path3) == true){
						user.getP().set(path3, false);
						user.saveP();
						p.sendMessage("§eVous perdez l'effet \"Régénération\"");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-3", true);
						user.getP().set(path1, false);
						user.getP().set(path2, false);
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§aVous regenerez 150% plus vite à partir de maintenant.");
						p.closeInventory();
					}
					break;
					
				case "§eRégénération x250%":
					if(user.getP().getBoolean(path4) == true){
						user.getP().set(path4, false);
						user.saveP();
						p.sendMessage("§eVous perdez l'effet \"Régénération\"");
						p.closeInventory();
					} else {
						user.getP().set(path+".level-4", true);
						user.getP().set(path1, false);
						user.getP().set(path2, false);
						user.getP().set(path3, false);
						user.saveP();
						p.sendMessage("§aVous regenerez 250% plus vite à partir de maintenant.");
						p.closeInventory();
					}
					break;
					
				case "§cSupprimer le bonus":
					user.getP().set(path+".level-1", false);
					user.getP().set(path+".level-2", false);
					user.getP().set(path+".level-3", false);
					user.getP().set(path+".level-4", false);
					user.saveP();
					p.sendMessage("§eVous perdez l'effet \"Régénération\" Votre Vitalité repasse à son état normal.");
					p.closeInventory();
					break;
					
				case "§7Retourner au panel":
					Panel.openStats(p);
					break;
					
				default:
					break;
				}
    		}
    	}
		
	}

}
