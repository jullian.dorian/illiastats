package fr.snyker.events.inventaire.stats;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.snyker.inventaire.Panel;
import fr.snyker.utils.StringName;

public class ActionVitesse implements Listener{
	
	@EventHandler
	public void actionVitesse(InventoryClickEvent event){
		
		Player p =  (Player) event.getWhoClicked();
		
		//Ont look le nom de l'inventaire
    	if(event.getInventory().getName().equals(StringName.panelVitesse)){
    		//SI le joueur clique dans du vide ou sur une case vide ont return
    		if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR){ return; }
    		//Ont empeche de cliquer sur les items
    		event.setCancelled(true);
    		
    		//Si l'item n'est pas nul est correspond au bon nom
    		if(event.getCurrentItem() != null && event.getCurrentItem().hasItemMeta()){
				switch(event.getCurrentItem().getItemMeta().getDisplayName()){
				
				case "§bVitesse x1.15":
					if(p.getWalkSpeed() >= 0.23){
						p.setWalkSpeed((float) 0.2);
						p.sendMessage("§cVous perdez l'effet §b\"Vitesse\". §eVotre vitesse repasse à son état normal.");
						p.closeInventory();
					} else {
						p.setWalkSpeed((float) (0.2*1.15));
						p.sendMessage("§aVotre vitesse à été améliorer, vous courez plus vite dorénavant !");
						p.closeInventory();
					}
					break;
					
				case "§bVitesse x1.25":
					if(p.getWalkSpeed() >= 0.25){
						p.setWalkSpeed((float) 0.2);
						p.sendMessage("§cVous perdez l'effet §b\"Vitesse\". §eVotre vitesse repasse à son état normal.");
						p.closeInventory();
					} else {
						p.setWalkSpeed((float) (0.2*1.25));
						p.sendMessage("§aVotre vitesse à été améliorer, vous courez plus vite dorénavant !");
						p.closeInventory();
					}
					break;
					
				case "§bVitesse x1.35":
					if(p.getWalkSpeed() >= 0.27){
						p.setWalkSpeed((float) 0.2);
						p.sendMessage("§cVous perdez l'effet §b\"Vitesse\". §eVotre vitesse repasse à son état normal.");
						p.closeInventory();
					} else {
						p.setWalkSpeed((float) (0.2*1.35));
						p.sendMessage("§aVotre vitesse à été améliorer, vous courez plus vite dorénavant !");
						p.closeInventory();
					}
					break;
					
				case "§bVitesse x1.5":
					if(p.getWalkSpeed() >= 0.3){
						p.setWalkSpeed((float) 0.2);
						p.sendMessage("§cVous perdez l'effet §b\"Vitesse\". §eVotre vitesse repasse à son état normal.");
						p.closeInventory();
					} else {
						p.setWalkSpeed((float) (0.2*1.5));
						p.sendMessage("§aVotre vitesse à été améliorer, vous courez plus vite dorénavant !");
						p.closeInventory();
					}
					break;
					
				case "§cSupprimer le bonus":
					p.setWalkSpeed((float) 0.2);
					p.sendMessage("§cVous perdez l'effet §b\"Vitesse\"");
					p.closeInventory();
					break;
					
				case "§7Retourner au panel":
					Panel.openStats(p);
					break;
					
				default:
					break;
				}
    		}
    	}
		
	}

}
