package fr.snyker.events.player;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import fr.snyker.IliaStats;
import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;

public class ActionInventory implements Listener{
	
	ConfigManager user = ConfigManager.getInstance();
	IliaStats config = IliaStats.iStats();
	
	@EventHandler
	public void PlayerInventory(InventoryClickEvent event){
		
		Player player = (Player) event.getWhoClicked();
		Inventory name = event.getInventory();
		
		if(player.getGameMode() == GameMode.CREATIVE){ return; }
		
		if(name != null){
    		if(config.getConfig().getBoolean("allow-interact") == false){
    			
    			if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR){ return; }
    			
    			if(event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().getDisplayName().equals(StringName.bookTitle)){
    				event.setCancelled(true);
    			}
    		} else {
    			event.setCancelled(false);
    		}
		}
		
	}
}
