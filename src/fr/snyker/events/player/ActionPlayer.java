package fr.snyker.events.player;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import fr.snyker.inventaire.Panel;
import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;

public class ActionPlayer implements Listener{

	ConfigManager user = ConfigManager.getInstance();
	
	@EventHandler
	public void InteractWithItem(PlayerInteractEvent e){
		Player p = e.getPlayer();
		
		if(e.getAction() == null) return;
		
		if(e.getAction() == Action.RIGHT_CLICK_AIR){

			
			if(e.getPlayer().getItemInHand().getType().equals(Material.ENCHANTED_BOOK)){
				if(e.getPlayer().getItemInHand().getItemMeta().getDisplayName() != null){
					if(p.getItemInHand().getItemMeta().getDisplayName().equals(StringName.bookTitle)){
						if(user.getP().contains(p.getUniqueId().toString())){
							Panel.openStats(p);
						} else {
							p.sendMessage("§cErreur: Vous n'êtes pas enregistrés dans nos fichiers de configuration.");
						}
						e.setCancelled(true);
					}
				}
			}
		}else{
			e.setCancelled(false);
		}
				
	}
	
}
