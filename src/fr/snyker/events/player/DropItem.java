package fr.snyker.events.player;

import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import fr.snyker.IliaStats;
import fr.snyker.utils.StringName;

public class DropItem implements Listener{
	
	IliaStats config = IliaStats.iStats();

	@EventHandler(priority = EventPriority.HIGHEST)
	public void DropItemPlayer(PlayerDropItemEvent e){
		
		if(e.getPlayer().getGameMode() == GameMode.SURVIVAL){
			if(e.getItemDrop().getItemStack().getItemMeta().getDisplayName().equals(StringName.bookTitle)){
				if(config.getConfig().getBoolean("allow-drop") == false){
					e.setCancelled(true);
				} else {
					e.setCancelled(false);
				}
			}
		}
	}

}
