package fr.snyker.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import fr.snyker.IliaStats;
import fr.snyker.utils.Threads;
import fr.snyker.utils.players.Create;

public class PlayerJoin implements Listener {
	
	IliaStats config = IliaStats.iStats();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		Player p = event.getPlayer();
		if(p.hasPermission("istats.create.onjoin")){
			Create.createPlayer(p, p.getName());
		}
		if(p.hasPermission("istats.book.receive.onjoin"))
			if(config.getConfig().get("slot-inventory") == "null"){
				p.getInventory().addItem(Threads.Book());
			} else {
				p.getInventory().setItem(config.getConfig().getInt("slot-inventory"), Threads.Book());
			}
		}

}
