package fr.snyker.events.player;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import fr.snyker.IliaStats;
import fr.snyker.management.ConfigManager;
import fr.snyker.utils.Threads;

public class PlayerVersusPlayer implements Listener{
	
	ConfigManager user = ConfigManager.getInstance();
	IliaStats config = IliaStats.iStats();
	
	@EventHandler
	public void PvP(EntityDeathEvent event){
		Entity ent = event.getEntity();
		
		if(config.getConfig().getBoolean("allow-drop") == false){
			event.getDrops().remove(Threads.Book());
		}
		
		if(ent == null){ return; }
		
		if(ent != null){
			Player killer = event.getEntity().getKiller();
			
			if(killer == null){ return; }
			
			int getPointEntity = config.getConfig().getInt("entity.player.points");
			int getKillEntity = config.getConfig().getInt("entity.player.kill");
			int getKillPlayer = user.getP().getInt(killer.getUniqueId().toString()+".infos.getkill");
			if(ent instanceof Player){
				if(getKillPlayer >= (getKillEntity-1)){
					Threads.addKillPlayer(killer, 1);
					Threads.addPoints(killer, getPointEntity);
					user.getP().set(killer.getUniqueId().toString()+".infos.getkill", 0);
					user.saveP();
					return;
				} 
				
				Threads.addKillPlayer(killer, 1);
				user.getP().set(killer.getUniqueId().toString()+".infos.getkill", getKillPlayer+1);
				user.saveP();
				return;
			}
		}
		
	}

}
/**
entity:
  player:
    #1 Kill pour 1 Point
    #kill: 1
    points: 1
    
    
    */
