package fr.snyker.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import fr.snyker.IliaStats;
import fr.snyker.utils.Threads;

public class RespawnPlayer implements Listener{

	IliaStats config = IliaStats.iStats();

	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerRespawn(PlayerRespawnEvent e){
		Player p = e.getPlayer();
		if(config.getConfig().getBoolean("give-book-on-respawn") == true){
			if(config.getConfig().get("slot-inventory") != "null"){
				p.getInventory().setItem(config.getConfig().getInt("slot-inventory"), Threads.Book());
			} else {
				p.getInventory().addItem(Threads.Book());
			}
		}
	}
	
}
