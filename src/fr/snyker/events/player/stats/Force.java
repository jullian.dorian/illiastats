package fr.snyker.events.player.stats;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import fr.snyker.management.ConfigManager;

public class Force implements Listener{
	
	ConfigManager user = ConfigManager.getInstance();
	
	@EventHandler
	public void setDamageByEntityAttack(EntityDamageByEntityEvent event){
		
		Entity p = event.getDamager();
//		Entity t = event.getEntity();
		
		String path1 = p.getUniqueId().toString()+".infos.stats.Force.level-1";
		String path2 = p.getUniqueId().toString()+".infos.stats.Force.level-2";
		String path3 = p.getUniqueId().toString()+".infos.stats.Force.level-3";
		String path4 = p.getUniqueId().toString()+".infos.stats.Force.level-4";
		
		if(event.getDamager().getType() == EntityType.PLAYER){
			 if(event.getCause() == DamageCause.ENTITY_ATTACK){
			      if(user.getP().getBoolean(path1) == true){
			    	  event.setDamage(event.getDamage()+(float)0.5);
			      } else 
				      if(user.getP().getBoolean(path2) == true){
				    	  event.setDamage(event.getDamage()+(float)1);
			      } else 
				      if(user.getP().getBoolean(path3) == true){
				    	  event.setDamage(event.getDamage()+(float)2);
			      } else 
				      if(user.getP().getBoolean(path4) == true){
				    	  event.setDamage(event.getDamage()+(float)4);
				  }
			 }
		}
	}
		
}
