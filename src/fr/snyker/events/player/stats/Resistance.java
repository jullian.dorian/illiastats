package fr.snyker.events.player.stats;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import fr.snyker.management.ConfigManager;

public class Resistance implements Listener{

	ConfigManager user = ConfigManager.getInstance();
	
	@EventHandler
	public void setResistance(EntityDamageByEntityEvent event){
		Entity p = event.getEntity();
//		Entity t = event.getDamager();
		
		String path1 = p.getUniqueId().toString()+".infos.stats.Resistance.level-1";
		String path2 = p.getUniqueId().toString()+".infos.stats.Resistance.level-2";
		String path3 = p.getUniqueId().toString()+".infos.stats.Resistance.level-3";
		String path4 = p.getUniqueId().toString()+".infos.stats.Resistance.level-4";
		
		if(event.getEntity().getType() != EntityType.PLAYER) return;
		
		if(event.getEntity().getType() == EntityType.PLAYER){
			if(event.getCause() == DamageCause.ENTITY_ATTACK){
				double damage = event.getDamage();
				if(user.getP().getBoolean(path1) == true){
			    	  event.setDamage(damage-(float)0.5);
			      } else 
				      if(user.getP().getBoolean(path2) == true){
				    	  event.setDamage(damage-(float)1);
			      } else 
				      if(user.getP().getBoolean(path3) == true){
				    	  event.setDamage(damage-(float)2);
			      } else 
				      if(user.getP().getBoolean(path4) == true){
				    	  event.setDamage(damage-(float)4);
				  }
			 }
		}
	}
}
