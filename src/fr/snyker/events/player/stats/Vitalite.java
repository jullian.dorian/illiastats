package fr.snyker.events.player.stats;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;

import fr.snyker.management.ConfigManager;

public class Vitalite implements Listener{
	
	ConfigManager user = ConfigManager.getInstance();
	
	@EventHandler
	public void regainHealth(EntityRegainHealthEvent event){
		EntityType p = event.getEntityType().PLAYER;
		Entity player = event.getEntity();
		
		String path1 = player.getUniqueId().toString()+".infos.stats.Vitalite.level-1";
		String path2 = player.getUniqueId().toString()+".infos.stats.Vitalite.level-2";
		String path3 = player.getUniqueId().toString()+".infos.stats.Vitalite.level-3";
		String path4 = player.getUniqueId().toString()+".infos.stats.Vitalite.level-4";
		
		if(p.isAlive()){
			if(event.getRegainReason() == RegainReason.SATIATED){
				if(user.getP().getBoolean(path1) == true){
					event.setAmount(1+(1*0.5));
				} else 
					if(user.getP().getBoolean(path2) == true){
						event.setAmount(1+1);
				} else 
					if(user.getP().getBoolean(path3) == true){
						event.setAmount(1+1.5);
				} else
					if(user.getP().getBoolean(path4) == true){
						event.setAmount(1+2.5);
				}
			}
		}
		
	}

}
