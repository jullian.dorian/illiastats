package fr.snyker.inventaire;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;
import fr.snyker.utils.Threads;

public class Panel {

	static ConfigManager user = ConfigManager.getInstance();
	
	private static Inventory stats;
	
	public static void openStats(Player p){
		stats = Bukkit.createInventory(null, 9*3, StringName.namePanel(p));
		
		//Agilite
		ArrayList<String> lore1 = new ArrayList<String>();
		lore1.add("     §bStats actuel : §c"+Threads.getPointAgi(p));
		lore1.add("");
		lore1.add("§a\u273f L'"+StringName.getNameAgi+" §evous permet de");
		lore1.add("§eprogresser dans le domaine du §cSaut.");
		lore1.add("");
		lore1.add("§7Clique gauche pour augmenter de 1 point");
		lore1.add("§7Clique droit pour augmenter de 5 points");
		lore1.add("§7Clique Shift+gauche pour augmenter tous les points");
		lore1.add("§7Clique molette pour afficher les paliers");
		//Force
		ArrayList<String> lore2 = new ArrayList<String>();
		lore2.add("       §eNiveau : §a"+Threads.getPointFor(p));
		lore2.add("");
		lore2.add("§c\u2734 La "+StringName.getNameFor+" §evous permet de progresser");
		lore2.add("§edans le domaine des §cEpées.");
		lore2.add("");
		lore2.add("§7Clique gauche pour augmenter de 1 point");
		lore2.add("§7Clique droit pour augmenter de 5 points");
		lore2.add("§7Clique Shift+gauche pour augmenter tous les points");
		lore2.add("§7Clique molette pour afficher les paliers");
		//Vitesse
		ArrayList<String> lore3 = new ArrayList<String>();
		lore3.add("       §eNiveau : §a"+Threads.getPointVit(p));
		lore3.add("");
		lore3.add("§b\u2747 La "+StringName.getNameVit+" §evous permet de progresser");
		lore3.add("§edans le domaine de la §cVitesse.");
		lore3.add("");
		lore3.add("§7Clique gauche pour augmenter de 1 point");
		lore3.add("§7Clique droit pour augmenter de 5 points");
		lore3.add("§7Clique Shift+gauche pour augmenter tous les points");
		lore3.add("§7Clique molette pour afficher les paliers");
		//Resistance
		ArrayList<String> lore4 = new ArrayList<String>();
		lore4.add("       §eNiveau : §a"+Threads.getPointRes(p));
		lore4.add("");
		lore4.add("§8\u2b22 La "+StringName.getNameRes+" §evous permet de progresser");
		lore4.add("§edans le domaine de la §cRésistance.");
		lore4.add("");
		lore4.add("§7Clique gauche pour augmenter de 1 point");
		lore4.add("§7Clique droit pour augmenter de 5 points");
		lore4.add("§7Clique Shift+gauche pour augmenter tous les points");
		lore4.add("§7Clique molette pour afficher les paliers");
		//Vitalite
		ArrayList<String> lore5 = new ArrayList<String>();
		lore5.add("       §eNiveau : §a"+Threads.getPointVita(p));
		lore5.add("");
		lore5.add("§6\u2764 La "+StringName.getNameVita+" §evous permet de gagner");
		lore5.add("§ede la §cRegénération §ede coeur.");
		lore5.add("");
		lore5.add("§7Clique gauche pour augmenter de 1 point");
		lore5.add("§7Clique droit pour augmenter de 5 points");
		lore5.add("§7Clique Shift+gauche pour augmenter tous les points");
		lore5.add("§7Clique molette pour afficher les paliers");
		
		
		ItemStack isc1 = Threads.newItem("§eAméliorez votre compétence en §a\u273f "+StringName.getNameAgi, lore1, Material.BOOK,  1);
		ItemStack isc2 = Threads.newItem("§eAméliorez votre compétence en §b\u2747 "+StringName.getNameVit, lore3, Material.BOOK, 1);
		ItemStack isc3 = Threads.newItem("§eAméliorez votre compétence en §c\u2734 "+StringName.getNameFor, lore2, Material.BOOK, 1);
		ItemStack isc4 = Threads.newItem("§eAméliorez votre compétence en §8\u2b22 "+StringName.getNameRes, lore4, Material.BOOK, 1);
		ItemStack isc5 = Threads.newItem("§eAméliorez votre compétence en §6\u2764 "+StringName.getNameVita, lore5, Material.BOOK, 1);
		
		ArrayList<String> headlore = new ArrayList<String>();
		headlore.add("");
		headlore.add("§f\u273f "+StringName.getNameAgi+" : §a"+Threads.getPointAgi(p));
		headlore.add("§f\u2747 "+StringName.getNameVit+" : §a"+Threads.getPointVit(p));
		headlore.add("§f\u2734 "+StringName.getNameFor+" : §a"+Threads.getPointFor(p));
		headlore.add("§f\u2b22 "+StringName.getNameRes+" : §a"+Threads.getPointRes(p));
		headlore.add("§f\u2764 "+StringName.getNameVita+" : §a"+Threads.getPointVita(p));
		ItemStack profil = Threads.newItemHead("§7Mon personnage", headlore, p.getName());
		
		ItemStack suc = Threads.newItem("§cCOMING SOON",  null, Material.BEACON,1);
		ItemStack definfo = Threads.newItem("§cCOMING SOON",  null, Material.LEATHER_CHESTPLATE,1);
		ItemStack statsinfo = Threads.newItem("§cCOMING SOON", null, Material.BOOK_AND_QUILL, 1);
		
		stats.setItem(2+9, isc1);
		stats.setItem(3+9, isc2);
		stats.setItem(4+9, isc3);
		stats.setItem(5+9, isc4);
		stats.setItem(6+9, isc5);
		stats.setItem(4+18, Threads.newItem("§cFermer le Panel", null, Material.BARRIER, 1));
		stats.setItem(8, suc);
		stats.setItem(0, statsinfo);
		stats.setItem(0+(9*2), definfo);
		stats.setItem(8+(9*2), profil);
		
		p.openInventory(stats);
		
	}
	
}
