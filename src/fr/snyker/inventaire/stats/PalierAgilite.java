package fr.snyker.inventaire.stats;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;
import fr.snyker.utils.Threads;

public class PalierAgilite {

	static ConfigManager user = ConfigManager.getInstance();
	
	private static Inventory panelAgilite;
	
	public static Inventory panelAgilite(Player player){
		panelAgilite = Bukkit.createInventory(null, 9*2, StringName.panelAgilite);
		ItemStack bg = Threads.newItem("§cPoints : "+Threads.getPointAgi(player), null, Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemStack lock = Threads.newItem("§cVérrouiller", null, Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemStack unlock = Threads.newItem("§aDévérouiller", null, Material.STAINED_GLASS_PANE, 1, (short) 5);
		
		ArrayList<String> agilite1 = new ArrayList<>();
		agilite1.add("    §c-=[ CONDITION ]=- ");
		agilite1.add("            §cAucune");
		
		ArrayList<String> agilite2 = new ArrayList<>();
		agilite2.add("    §c-=[ CONDITION ]=- ");
		agilite2.add("            §cAucune");
		
		ArrayList<String> agilite3 = new ArrayList<>();
		agilite3.add("    §c-=[ CONDITION ]=- ");
		agilite3.add("            §cAucune");
		
		ArrayList<String> agilite4 = new ArrayList<>();
		agilite4.add("    §c-=[ CONDITION ]=- ");
		agilite4.add("            §cAucune");
		
		for(int a = 0; a < 9*2;){
			panelAgilite.setItem(a++, bg);
		}
		
		if(user.getP().contains(player.getUniqueId().toString())){
			if(Threads.getPointAgi(player) >= 50){
				panelAgilite.setItem(1, unlock);
				panelAgilite.setItem(1+9, Threads.newItem("§bJump x1.5", agilite1, Material.FEATHER, 1));
			} else {
				panelAgilite.setItem(1, lock);
				panelAgilite.setItem(1+9, Threads.newItem("§cVérrouiller §7[Débloquer à 50 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointAgi(player) >= 100){
				panelAgilite.setItem(3, unlock);
				panelAgilite.setItem(3+9, Threads.newItem("§bJump x2", agilite2, Material.FEATHER, 1));
			}  else {
				panelAgilite.setItem(3, lock);
				panelAgilite.setItem(3+9, Threads.newItem("§cVérrouiller §7[Débloquer à 100 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointAgi(player) >= 150){
				panelAgilite.setItem(5, unlock);	
				panelAgilite.setItem(5+9, Threads.newItem("§bJump x3", agilite3, Material.FEATHER, 1));
			} else {
				panelAgilite.setItem(5, lock);
				panelAgilite.setItem(5+9, Threads.newItem("§cVérrouiller §7[Débloquer à 150 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointAgi(player) >= 200){
				panelAgilite.setItem(7, unlock);	
				panelAgilite.setItem(7+9, Threads.newItem("§bJump x4", agilite4, Material.FEATHER, 1));
			} else {
				panelAgilite.setItem(7, lock);
				panelAgilite.setItem(7+9, Threads.newItem("§cVérrouiller §7[Débloquer à 200 Points]", null, Material.BARRIER, 1));
			}
		}
		
		if(player.hasPotionEffect(PotionEffectType.JUMP)){
			panelAgilite.setItem(4, Threads.newItem("§cSupprimer le bonus", null, Material.BARRIER, 1));
		}
		panelAgilite.setItem(4+9, Threads.newItem("§7Retourner au panel", null, Material.ARROW, 1));
		
		return panelAgilite;
		
	}
	
}
