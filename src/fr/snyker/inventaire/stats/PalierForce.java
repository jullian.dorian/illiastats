package fr.snyker.inventaire.stats;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;
import fr.snyker.utils.Threads;

public class PalierForce {

	static ConfigManager user = ConfigManager.getInstance();
	
	private static Inventory panelForce;
	
	public static Inventory panelForce(Player player){
		
		String path1 = player.getUniqueId().toString()+".infos.stats.Force.level-1";
		String path2 = player.getUniqueId().toString()+".infos.stats.Force.level-2";
		String path3 = player.getUniqueId().toString()+".infos.stats.Force.level-3";
		String path4 = player.getUniqueId().toString()+".infos.stats.Force.level-4";
		
		panelForce = Bukkit.createInventory(null, 9*2, StringName.panelForce);
		ItemStack bg = Threads.newItem("§cPoints : "+Threads.getPointFor(player), null, Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemStack lock = Threads.newItem("§cVérrouiller", null, Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemStack unlock = Threads.newItem("§aDévérouiller", null, Material.STAINED_GLASS_PANE, 1, (short) 5);
		
		ArrayList<String> force1 = new ArrayList<>();
		force1.add("    §c-=[ CONDITION ]=- ");
		force1.add("            §cAucune");
		
		ArrayList<String> force2 = new ArrayList<>();
		force2.add("    §c-=[ CONDITION ]=- ");
		force2.add("            §cAucune");
		
		ArrayList<String> force3 = new ArrayList<>();
		force3.add("    §c-=[ CONDITION ]=- ");
		force3.add("            §cAucune");
		
		ArrayList<String> force4 = new ArrayList<>();
		force4.add("    §c-=[ CONDITION ]=- ");
		force4.add("            §cAucune");
		
		for(int a = 0; a < 9*2;){
			panelForce.setItem(a++, bg);
		}
		
		if(user.getP().contains(player.getUniqueId().toString())){
			if(Threads.getPointFor(player) >= 50){
				panelForce.setItem(1, unlock);
				panelForce.setItem(1+9, Threads.newItem("§5Dégâts +0.5", force1, Material.WOOD_SWORD, 1));
			} else {
				panelForce.setItem(1, lock);
				panelForce.setItem(1+9, Threads.newItem("§cVérrouiller §7[Débloquer à 50 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointFor(player) >= 100){
				panelForce.setItem(3, unlock);	
				panelForce.setItem(3+9, Threads.newItem("§5Dégâts +1", force2, Material.IRON_SWORD, 1));
			}  else {
				panelForce.setItem(3, lock);
				panelForce.setItem(3+9, Threads.newItem("§cVérrouiller §7[Débloquer à 100 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointFor(player) >= 150){
				panelForce.setItem(5, unlock);	
				panelForce.setItem(5+9, Threads.newItem("§5Dégâts +2", force3, Material.GOLD_SWORD, 1));
			} else {
				panelForce.setItem(5, lock);
				panelForce.setItem(5+9, Threads.newItem("§cVérrouiller §7[Débloquer à 150 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointFor(player) >= 200){
				panelForce.setItem(7, unlock);
				panelForce.setItem(7+9, Threads.newItem("§5Dégâts +4", force4, Material.DIAMOND_SWORD, 1));
			} else {
				panelForce.setItem(7, lock);
				panelForce.setItem(7+9, Threads.newItem("§cVérrouiller §7[Débloquer à 200 Points]", null, Material.BARRIER, 1));
			}
		}
		
		if(user.getP().getBoolean(path1) == true || user.getP().getBoolean(path2) == true || user.getP().getBoolean(path3) == true || user.getP().getBoolean(path4) == true){
			panelForce.setItem(4, Threads.newItem("§cSupprimer le bonus", null, Material.BARRIER, 1));
	    } 
		panelForce.setItem(4+9, Threads.newItem("§7Retourner au panel", null, Material.ARROW, 1));
		
		return panelForce;
		
	}
	
}
