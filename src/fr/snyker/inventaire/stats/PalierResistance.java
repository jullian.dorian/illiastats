package fr.snyker.inventaire.stats;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;
import fr.snyker.utils.Threads;

public class PalierResistance {

	static ConfigManager user = ConfigManager.getInstance();
	
	private static Inventory panelResistance;
	
	public static Inventory panelResistance(Player player){
		
		String path1 = player.getUniqueId().toString()+".infos.stats.Resistance.level-1";
		String path2 = player.getUniqueId().toString()+".infos.stats.Resistance.level-2";
		String path3 = player.getUniqueId().toString()+".infos.stats.Resistance.level-3";
		String path4 = player.getUniqueId().toString()+".infos.stats.Resistance.level-4";
		
		panelResistance = Bukkit.createInventory(null, 9*2, StringName.panelResistance);
		ItemStack bg = Threads.newItem("§cPoints : "+Threads.getPointRes(player), null, Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemStack lock = Threads.newItem("§cVérrouiller", null, Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemStack unlock = Threads.newItem("§aDévérouiller", null, Material.STAINED_GLASS_PANE, 1, (short) 5);
		
		ArrayList<String> resist1 = new ArrayList<>();
		resist1.add("    §c-=[ CONDITION ]=- ");
		resist1.add("            §cAucune");
		
		ArrayList<String> resist2 = new ArrayList<>();
		resist2.add("    §c-=[ CONDITION ]=- ");
		resist2.add("            §cAucune");
		
		ArrayList<String> resist3 = new ArrayList<>();
		resist3.add("    §c-=[ CONDITION ]=- ");
		resist3.add("            §cAucune");
		
		ArrayList<String> resist4 = new ArrayList<>();
		resist4.add("    §c-=[ CONDITION ]=- ");
		resist4.add("            §cAucune");
		
		for(int a = 0; a < 9*2;){
			panelResistance.setItem(a++, bg);
		}
		
		if(user.getP().contains(player.getUniqueId().toString())){
			if(Threads.getPointRes(player) >= 50){
				panelResistance.setItem(1, unlock);
				panelResistance.setItem(1+9, Threads.newItem("§8Réduire de -0.5", resist1, Material.LEATHER_CHESTPLATE, 1));
			} else {
				panelResistance.setItem(1, lock);
				panelResistance.setItem(1+9, Threads.newItem("§cVérrouiller §7[Débloquer à 50 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointRes(player) >= 100){
				panelResistance.setItem(3, unlock);	
				panelResistance.setItem(3+9, Threads.newItem("§8Réduire de -1", resist2, Material.IRON_CHESTPLATE, 1));
			}  else {
				panelResistance.setItem(3, lock);
				panelResistance.setItem(3+9, Threads.newItem("§cVérrouiller §7[Débloquer à 100 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointRes(player) >= 150){
				panelResistance.setItem(5, unlock);
				panelResistance.setItem(5+9, Threads.newItem("§8Réduire de -2", resist3, Material.GOLD_CHESTPLATE, 1));
			} else {
				panelResistance.setItem(5, lock);
				panelResistance.setItem(5+9, Threads.newItem("§cVérrouiller §7[Débloquer à 150 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointRes(player) >= 200){
				panelResistance.setItem(7, unlock);	
				panelResistance.setItem(7+9, Threads.newItem("§8Réduire de -4", resist4, Material.DIAMOND_CHESTPLATE, 1));
			} else {
				panelResistance.setItem(7, lock);
				panelResistance.setItem(7+9, Threads.newItem("§cVérrouiller §7[Débloquer à 200 Points]", null, Material.BARRIER, 1));
			}
		}
		
		if(user.getP().getBoolean(path1) == true || user.getP().getBoolean(path2) == true || user.getP().getBoolean(path3) == true || user.getP().getBoolean(path4) == true){
			panelResistance.setItem(4, Threads.newItem("§cSupprimer le bonus", null, Material.BARRIER, 1));
		}
		
		panelResistance.setItem(4+9, Threads.newItem("§7Retourner au panel", null, Material.ARROW, 1));
		
		return panelResistance;
		
	}
	
}
