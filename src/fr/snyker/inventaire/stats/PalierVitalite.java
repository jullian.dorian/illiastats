package fr.snyker.inventaire.stats;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;
import fr.snyker.utils.Threads;

public class PalierVitalite {

	static ConfigManager user = ConfigManager.getInstance();
	
	private static Inventory panelVitalite;
	
	public static Inventory panelVitalite(Player player){
		
		String path1 = player.getUniqueId().toString()+".infos.stats.Vitalite.level-1";
		String path2 = player.getUniqueId().toString()+".infos.stats.Vitalite.level-2";
		String path3 = player.getUniqueId().toString()+".infos.stats.Vitalite.level-3";
		String path4 = player.getUniqueId().toString()+".infos.stats.Vitalite.level-4";
		
		panelVitalite = Bukkit.createInventory(null, 9*2, StringName.panelVitalite);
		ItemStack bg = Threads.newItem("§cPoints : "+Threads.getPointVita(player), null, Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemStack lock = Threads.newItem("§cVérrouiller", null, Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemStack unlock = Threads.newItem("§aDévérouiller", null, Material.STAINED_GLASS_PANE, 1, (short) 5);
		
		ArrayList<String> regen1 = new ArrayList<>();
		regen1.add("    §c-=[ CONDITION ]=- ");
		regen1.add("    §cBarre de faim §fmax");
		
		ArrayList<String> regen2 = new ArrayList<>();
		regen2.add("    §c-=[ CONDITION ]=- ");
		regen2.add("    §cBarre de faim §fmax");
		
		ArrayList<String> regen3 = new ArrayList<>();
		regen3.add("    §c-=[ CONDITION ]=- ");
		regen3.add("    §cBarre de faim §fmax");
		
		ArrayList<String> regen4 = new ArrayList<>();
		regen4.add("    §c-=[ CONDITION ]=- ");
		regen4.add("    §cBarre de faim §fmax");
		
		for(int a = 0; a < 9*2;){
			panelVitalite.setItem(a++, bg);
		}
		
		if(user.getP().contains(player.getUniqueId().toString())){
			if(Threads.getPointVita(player) >= 100){
				panelVitalite.setItem(1, unlock);
				panelVitalite.setItem(1+9, Threads.newItem("§eRégénération x50%", regen1, Material.APPLE, 1));
			} else {
				panelVitalite.setItem(1, lock);
				panelVitalite.setItem(1+9, Threads.newItem("§cVérrouiller §7[Débloquer à 100 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointVita(player) >= 200){
				panelVitalite.setItem(3, unlock);
				panelVitalite.setItem(3+9, Threads.newItem("§eRégénération x100%", regen2, Material.APPLE, 1));
			}  else {
				panelVitalite.setItem(3, lock);
				panelVitalite.setItem(3+9, Threads.newItem("§cVérrouiller §7[Débloquer à 200 Points]", null , Material.BARRIER, 1));
			}
			
			if(Threads.getPointVita(player) >= 300){
				panelVitalite.setItem(5, unlock);
				panelVitalite.setItem(5+9, Threads.newItem("§eRégénération x150%", regen3, Material.APPLE, 1));
			} else {
				panelVitalite.setItem(5, lock);
				panelVitalite.setItem(5+9, Threads.newItem("§cVérrouiller §7[Débloquer à 300 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointVita(player) >= 400){
				panelVitalite.setItem(7, unlock);	
				panelVitalite.setItem(7+9, Threads.newItem("§eRégénération x250%", regen4, Material.APPLE, 1));
			} else {
				panelVitalite.setItem(7, lock);
				panelVitalite.setItem(7+9, Threads.newItem("§cVérrouiller §7[Débloquer à 400 Points]", null, Material.BARRIER, 1));
			}
		}
		if(user.getP().getBoolean(path1) == true || user.getP().getBoolean(path2) == true || user.getP().getBoolean(path3) == true || user.getP().getBoolean(path4) == true){
			 panelVitalite.setItem(4, Threads.newItem("§cSupprimer le bonus", null, Material.BARRIER, 1));
		}
		
		panelVitalite.setItem(4+9, Threads.newItem("§7Retourner au panel", null, Material.ARROW, 1));
		
		return panelVitalite;
		
	}
	
}
