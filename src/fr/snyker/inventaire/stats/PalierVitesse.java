package fr.snyker.inventaire.stats;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.snyker.management.ConfigManager;
import fr.snyker.utils.StringName;
import fr.snyker.utils.Threads;

public class PalierVitesse {
	
	static ConfigManager user = ConfigManager.getInstance();
	
	private static Inventory panelVitesse;
	
	public static Inventory panelVitesse(Player player){
		panelVitesse = Bukkit.createInventory(null, 9*2, StringName.panelVitesse);
		ItemStack bg = Threads.newItem("§cPoints : "+Threads.getPointVit(player), null, Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemStack lock = Threads.newItem("§cVérrouiller", null, Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemStack unlock = Threads.newItem("§aDévérouiller", null, Material.STAINED_GLASS_PANE, 1, (short) 5);
		
		for(int a = 0; a < 9*2;){
			panelVitesse.setItem(a++, bg);
		}
		
		ArrayList<String> vitesse1 = new ArrayList<>();
		vitesse1.add("    §c-=[ CONDITION ]=- ");
		vitesse1.add("            §cAucune");
		
		ArrayList<String> vitesse2 = new ArrayList<>();
		vitesse2.add("    §c-=[ CONDITION ]=- ");
		vitesse2.add("            §cAucune");
		
		ArrayList<String> vitesse3 = new ArrayList<>();
		vitesse3.add("    §c-=[ CONDITION ]=- ");
		vitesse3.add("            §cAucune");
		
		ArrayList<String> vitesse4 = new ArrayList<>();
		vitesse4.add("    §c-=[ CONDITION ]=- ");
		vitesse4.add("            §cAucune");
		
		if(user.getP().contains(player.getUniqueId().toString())){
			if(Threads.getPointVit(player) >= 50){
				panelVitesse.setItem(1, unlock);
				panelVitesse.setItem(1+9, Threads.newItem("§bVitesse x1.15", vitesse1, Material.FEATHER, 1));
			} else {
				panelVitesse.setItem(1, lock);
				panelVitesse.setItem(1+9, Threads.newItem("§cVérrouiller §7[Débloquer à 50 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointVit(player) >= 100){
				panelVitesse.setItem(3, unlock);	
				panelVitesse.setItem(3+9, Threads.newItem("§bVitesse x1.25", vitesse2, Material.FEATHER, 1));
			}  else {
				panelVitesse.setItem(3, lock);
				panelVitesse.setItem(3+9, Threads.newItem("§cVérrouiller §7[Débloquer à 100 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointVit(player) >= 150){
				panelVitesse.setItem(5, unlock);	
				panelVitesse.setItem(5+9, Threads.newItem("§bVitesse x1.35", vitesse3, Material.FEATHER, 1));
			} else {
				panelVitesse.setItem(5, lock);
				panelVitesse.setItem(5+9, Threads.newItem("§cVérrouiller §7[Débloquer à 150 Points]", null, Material.BARRIER, 1));
			}
			
			if(Threads.getPointVit(player) >= 200){
				panelVitesse.setItem(7, unlock);
				panelVitesse.setItem(7+9, Threads.newItem("§bVitesse x1.5", vitesse4, Material.FEATHER, 1));
			} else {
				panelVitesse.setItem(7, lock);
				panelVitesse.setItem(7+9, Threads.newItem("§cVérrouiller §7[Débloquer à 200 Points]", null, Material.BARRIER, 1));
			}
			
			if(player.getWalkSpeed() >= 0.21){
				panelVitesse.setItem(4, Threads.newItem("§cSupprimer le bonus", null, Material.BARRIER, 1));
			}
			
			panelVitesse.setItem(4+9, Threads.newItem("§7Retourner au panel", null, Material.ARROW, 1));
		}
		
		
		
		return panelVitesse;
		
	}

}
