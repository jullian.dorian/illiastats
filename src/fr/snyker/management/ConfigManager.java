package fr.snyker.management;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class ConfigManager {
	
	private ConfigManager(){}
	static ConfigManager instance = new ConfigManager();
	
	public static ConfigManager getInstance(){
		return instance;
	}
	
	FileConfiguration player;
	File pfile;

	
	public void setup(Plugin pl){
		if(!pl.getDataFolder().exists()){
			pl.getDataFolder().mkdirs();
		}
		
		pfile = new File(pl.getDataFolder(), "user_data.yml");
	
		player = YamlConfiguration.loadConfiguration(pfile);
		
		if(!pfile.exists()){
			try{
				pfile.createNewFile();
			} catch(IOException e){
				Bukkit.getServer().getLogger().severe("Impossible de crée user_data.yml");
			}
		}
		
	}

	public FileConfiguration getP(){
		return player;
	}
	
	public void saveP(){
		try{
			player.save(pfile);
		} catch(IOException e){
			Bukkit.getServer().getLogger().severe("Impossible de sauvegarder user-data.yml");
		}
	}
		
	public void reloadP(){
		player = YamlConfiguration.loadConfiguration(pfile);
	}
	
	
}
