package fr.snyker.utils;

import org.bukkit.entity.Player;

public class StringName {
	
	public static String bookTitle = "§bMes Infos et Compétences";
	public static String panelVitesse = "§7> §ePalier §7> §bVitesse";
	public static String panelAgilite = "§7> §ePalier §7> §bAgilite";
	public static String panelForce = "§7> §ePalier §7> §bForce";
	public static String panelResistance = "§7> §ePalier §7> §bResistance";
	public static String panelVitalite = "§7> §ePalier §7> §bVitalite";
	
	public static String namePanel(Player p){
		return "§c-=[-| "+Threads.getPoints(p)+" Points restants";
	}

	public static String getNameAgi = "Agilite";
	public static String getNameFor = "Force";
	public static String getNameVita = "Vitalite";
	public static String getNameRes = "Resistance";
	public static String getNameVit = "Vitesse";

}
