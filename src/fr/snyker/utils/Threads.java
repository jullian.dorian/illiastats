package fr.snyker.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import fr.snyker.management.ConfigManager;

public class Threads {
	
	static ConfigManager user = ConfigManager.getInstance();
	
	/**
	 * @param Ont regarde les points du joueurs.
	 * @return Ont lui return les points.
	 */
	public static int getPoints(Player player){
		return user.getP().getInt(player.getUniqueId().toString()+".infos.points");
	}
	public static void addPoints(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".infos.points", Threads.getPoints(player)+somme);
		user.saveP();
	}
	public static void removePoints(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".infos.points", Threads.getPoints(player)-somme);
		user.saveP();
	}
	
	public static int getPointAgi(Player player){
		return user.getP().getInt(player.getUniqueId().toString()+".caracteristiques.Agilite");
	}
	public static int getPointVit(Player player){
		return user.getP().getInt(player.getUniqueId().toString()+".caracteristiques.Vitesse");
	}
	public static int getPointFor(Player player){
		return user.getP().getInt(player.getUniqueId().toString()+".caracteristiques.Force");
	}
	public static int getPointRes(Player player){
		return user.getP().getInt(player.getUniqueId().toString()+".caracteristiques.Resistance");
	}
	public static int getPointVita(Player player){
		return user.getP().getInt(player.getUniqueId().toString()+".caracteristiques.Vitalite");
	}
	
	public static void addPointAgi(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".caracteristiques.Agilite", getPointAgi(player) + somme);
		user.saveP();
	}
	public static void addPointVit(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".caracteristiques.Vitesse", getPointVit(player) + somme);
		user.saveP();
	}
	public static void addPointFor(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".caracteristiques.Force", getPointFor(player) + somme);
		user.saveP();
	}
	public static void addPointRes(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".caracteristiques.Resistance", getPointRes(player) + somme);
		user.saveP();
	}
	public static void addPointVita(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".caracteristiques.Vitalite", getPointVita(player) + somme);
		user.saveP();
	}
	
	public static void removePointAgi(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".caracteristiques.Agilite", getPointAgi(player) - somme);
		user.saveP();
	}
	public static void removePointVit(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".caracteristiques.Vitesse", getPointVit(player) - somme);
		user.saveP();
	}
	public static void removePointFor(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".caracteristiques.Force", getPointFor(player) - somme);
		user.saveP();
	}
	public static void removePointRes(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".caracteristiques.Resistance", getPointRes(player) - somme);
		user.saveP();
	}
	public static void removePointVita(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".caracteristiques.Vitalite", getPointVita(player) - somme);
		user.saveP();
	}
	
	public static int getKillPlayer(Player player){
		return user.getP().getInt(player.getUniqueId().toString()+".infos.joueurs-tuee");
	}
	public static void addKillPlayer(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".infos.joueurs-tuee", getKillPlayer(player)+somme);
		user.saveP();
	}
	public static void removeKillPlayer(Player player, int somme){
		user.getP().set(player.getUniqueId().toString()+".infos.joueurs-tuee", getKillPlayer(player)-somme);
		user.saveP();
	}
	
	public static ItemStack newItem(String name, ArrayList<String> test, Material material, int quantity){
		ItemStack item = new ItemStack(material, quantity);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(name);
		itemMeta.setLore(test);
		
		item.setItemMeta(itemMeta);
		return item;
	}
	
	public static ItemStack newItem(String name, ArrayList<String> test, Material material, int quantity, short data){
		ItemStack item = new ItemStack(material, quantity, data);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(name);
		itemMeta.setLore(test);
		
		item.setItemMeta(itemMeta);
		return item;
	}
	
	
	public static ItemStack newItemHead(String name, ArrayList<String> lore, String pseudo){
		ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta itemMeta = (SkullMeta) item.getItemMeta();
		itemMeta.setDisplayName(name);
		itemMeta.setLore(lore);
		itemMeta.setOwner(pseudo);
		
		item.setItemMeta(itemMeta);
		return item;
	}
	
	public static ItemStack Book(){
		ArrayList<String> args = new ArrayList<>();
		args.add(""); 
		args.add("§7Clique pour voir t'es informations"); 
		args.add("§7et gerer ton panel de compétences.");
		
		ItemStack book = newItem(StringName.bookTitle, args, Material.ENCHANTED_BOOK, 1);
		return book;
	}

}
/**
 * Exemple du fichier :
 * UUID:
 *   infos:
 *     name: Snyker
 *     points: X
 *     creatures-tuee: X
 *     joueurs-tuee: X
 *     creature:
 *       Sheep:
 *         tuee: X
 *         actuel: X
 *   caracteristiques:
 *     Agilite: X
 *     Vitesse: X
 *     Force: X
 *     Resistance: X
 *     Vitalite: X 
 */  
