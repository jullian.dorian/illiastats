package fr.snyker.utils;

import org.bukkit.entity.Player;

import fr.snyker.IliaStats;

public class UtilsCommand {

	static IliaStats config = IliaStats.iStats();
	
	public static void giveBook(Player player){
		if(player.hasPermission("istats.book.give")){
			if(!player.getInventory().contains(Threads.Book())){
				if(config.getConfig().getBoolean("allow-interact") == true && config.getConfig().get("slot-inventory") == "null"){
					player.getInventory().addItem(Threads.Book());
				} else {
					player.getInventory().setItem(config.getConfig().getInt("slot-inventory"), Threads.Book());
				}
				player.sendMessage("§eVoici le livre des compétences. Faites en bon usage !");
			}else{
				player.sendMessage("§cErreur: Il semblerait vous possèdez déjà le livre.");
			}
		} else {
			player.sendMessage("§cErreur: Vous ne possedez la permission pour éxécuter cette commande.");
		}
	}

	public static void removeBook(Player player) {
		if(player.hasPermission("istats.book.remove")){
			if(player.getInventory().contains(Threads.Book())){
				player.getInventory().removeItem(Threads.Book());
				player.sendMessage("§eVous venez de supprimer le livre de votre inventaire.");
			} else {
				player.sendMessage("§cErreur: Vous n'avez pas le livre dans votre inventaire !");
			}
		} else {
			player.sendMessage("§cErreur: Vous ne possedez la permission pour éxécuter cette commande.");
		}
	}
	
	
}
