package fr.snyker.utils.command;

import org.bukkit.entity.Player;

public class Help {
	/** ENVOIE LA LISTE DES COMMANDES */
	public static void commandHelp(Player player){
		if(player.isOp()){
			player.sendMessage("§8-=-=-=-=-=-=-=[ §7§nEfusStats Commandes Admin§r§8 ]=-=-=-=-=-=-=-");
			player.sendMessage("§f/istats §7: §eAffiche votre carte personnel.");
			player.sendMessage("§f/istats §bsee §f<player> §7: §eAffiche la carte d'un joueur.");
			player.sendMessage("§f/istats §cremove §f<player> §7: §eSupprime §centièrement un joueur");
			player.sendMessage("§f/istats §acreate §7: §eVous créer un compte.");
			player.sendMessage("§f/istats §9book [give|remove] §7: §eVous donne le livre des compétences.");
			player.sendMessage("§f/istats §2menu §7: §eVous ouvre le panel des compétences.");
			player.sendMessage("§f/istats §freload §7: §eRedémarre les configurations.");
		} else {
			player.sendMessage("§8-=-=-=-=-=-=-=-=[ §7§nEfusStats Commandes§r§8 ]=-=-=-=-=-=-=-=-");
			player.sendMessage("§f/istats §7: §eAffiche votre carte personnel.");
			if(player.hasPermission("istats.see.player")){
				player.sendMessage("§f/istats §bsee §f<player> §7: §eAffiche la carte d'un joueur.");
			}
			if(player.hasPermission("istats.create")){
				player.sendMessage("§f/istats §acreate §7: §eCréer un compte de caractéristiques.");
			}
			if(player.hasPermission("istats.remove")){
				player.sendMessage("§f/istats §cremove §7: §eSupprimer votre compte de caractéristiques.");
			} else if(player.hasPermission("istats.remove.player")){
				player.sendMessage("§f/istats §cremove §f<player> §7: §eSupprime §centièrement un joueur");
			}
			if(player.hasPermission("istats.book.give") || player.hasPermission("istats.book.remove")){
				player.sendMessage("§f/istats §9book [give|remove] §7: §eVous donne/supprime le livre des compétences.");
			}
			if(player.hasPermission("istats.menu")){
				player.sendMessage("§f/istats §2menu §7: §eVous ouvre le panel des compétences.");
			}
			if(player.hasPermission("istats.reload")){
				player.sendMessage("§f/istats §freload §7: §eRedémarre les configurations.");
			}
			
		}
	}

}
