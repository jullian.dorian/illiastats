package fr.snyker.utils.command;

import org.bukkit.entity.Player;

import fr.snyker.management.ConfigManager;
import fr.snyker.utils.Threads;

public class SeePlayer {

	static ConfigManager user = ConfigManager.getInstance();
	
	public static void seePlayer(Player player, Player target) {
			if(target != null){
				if(user.getP().contains(target.getUniqueId().toString())){
					player.sendMessage("§8§m================================================");
					player.sendMessage("§aLe joueur à : §e" + user.getP().getInt(target.getUniqueId().toString()+".infos.points") +" §bpoints de compétences.");
					player.sendMessage("§8§m================================================");
					player.sendMessage("§aAgilité : §e" + Threads.getPointAgi(target) +" §bpoints.");
					player.sendMessage("§aVitesse : §e" + Threads.getPointVit(target) +" §bpoints.");
					player.sendMessage("§aForce : §e" + Threads.getPointFor(target) +" §bpoints.");
					player.sendMessage("§aRésistance : §e" + Threads.getPointRes(target) +" §bpoints.");
					player.sendMessage("§aVitalité : §e" + Threads.getPointVita(target) +" §bpoints.");
				} else {
					player.sendMessage("§cErreur: Le joueur n'existe pas dans nos fichiers de configuration.");
				}
			} else {
				player.sendMessage("§cErreur: Le joueur n'est pas en ligne ou n'existe pas.");
			}
	}

}
