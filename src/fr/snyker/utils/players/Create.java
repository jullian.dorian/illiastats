package fr.snyker.utils.players;

import org.bukkit.entity.Player;

import fr.snyker.IliaStats;
import fr.snyker.management.ConfigManager;
import fr.snyker.utils.Threads;

public class Create {

	static ConfigManager user = ConfigManager.getInstance();
	static IliaStats config = IliaStats.iStats();
	
	/** CREATION DU JOUEUR DANS LA SAUVEGARDE  */
	public static void createPlayer(Player player, String name){
		String uuid = player.getUniqueId().toString();
		
			if(!user.getP().contains(uuid)){
				//Ont crée la section du joueur, sont ID
				user.getP().createSection(uuid);
				
				//On lui ajoute tout ce qu'il faut.
				user.getP().set(uuid+".infos.name", name);
				user.getP().set(uuid+".infos.points", 0);
//				user.getP().set(uuid+".infos.creatures-tuee", player.getStatistic(Statistic.MOB_KILLS));
				user.getP().set(uuid+".infos.joueurs-tuee", 0);
				user.getP().set(uuid+".infos.getkill", 0);
				
				//Gestion des caracs
				user.getP().createSection(uuid+".caracteristiques");
				user.getP().set(uuid+".caracteristiques.Agilite", 0);
				user.getP().set(uuid+".caracteristiques.Vitesse", 0);
				user.getP().set(uuid+".caracteristiques.Force", 0);
				user.getP().set(uuid+".caracteristiques.Resistance", 0);
				user.getP().set(uuid+".caracteristiques.Vitalite", 0);
				user.saveP();
				
				player.getInventory().setItem(config.getConfig().getInt("slot-inventory"), Threads.Book());
				
				player.sendMessage("§aVous venez de crée votre compte.");
				player.sendMessage("§ePour consulter vos compétences clique droit avec le livre spécifique.");
			} else {
				player.sendMessage("§cVous possedez déjà un compte !");
			}
		
	}
	
}
/**
 * Exemple du fichier :
 * UUID:
 *   infos:
 *     name: Snyker
 *     points: X
 *     creatures-tuee: X
 *     joueurs-tuee: X
 *     creature:
 *       Sheep:
 *         tuee: X
 *         actuel: X
 *   caracteristiques:
 *     Agilite: X
 *     Vitesse: X
 *     Force: X
 *     Resistance: X
 *     Vitalite: X 
 */     
