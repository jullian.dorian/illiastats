package fr.snyker.utils.players;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.snyker.management.ConfigManager;

public class Remove {

	static ConfigManager user = ConfigManager.getInstance();
	
	public static void removePlayer(Player player, String[] args) {
		String uuid = player.getUniqueId().toString();
		
		if(args.length == 1){
			if(player.hasPermission("istats.remove.me")){
				//Le joueur à la permissions alors ont execute le code
				if(user.getP().contains(uuid)){
					user.getP().set(uuid, null);
					user.saveP();
					player.sendMessage("§aVous vous êtes supprimés des fichiers de configuration.");
					player.sendMessage("§aPour crée un compte : §f/stats create");
				//Le joueur n'existe pas dans la config
				} else {
					player.sendMessage("§cErreur: Vous n'êtes pas enregistrés dans nos fichiers de configuration.");
					player.sendMessage("§aPour crée un compte : §f/stats create");
				}
			
				//Le joueur n'a pas la permissions POUR CE SUPPRIMER LUI MÊME
			} else {
				player.sendMessage("§cErreur: Vous ne possedez la permission pour éxécuter cette commande.");
			}
		} else if(args.length == 2){
			if(player.hasPermission("istats.remove.player")){
				Player target = Bukkit.getPlayerExact(args[1]);
				if(target != null){
					if(user.getP().contains(target.getUniqueId().toString())){
						user.getP().set(target.getUniqueId().toString(), null);
						user.saveP();
						player.sendMessage("§aVous venez de supprimer le joueur : §f"+target.getName()+"§a de nos fichiers de configuration.");
						
					//Le joueur n'existe pas dans la config
					} else {
						player.sendMessage("§cErreur: §f"+target.getName()+" §cn'est pas enregistrés dans nos fichiers de configuration.");
					}
				
				//LE JOUEUR EST DECONNECTER
				} else {
					player.sendMessage("§cErreur: Le joueur est déconnecter ou introuvable.");
				}
				
				//Le joueur n'a pas la permissions pour supprimé un joueur
			} else {
				player.sendMessage("§cErreur: Vous ne possedez la permission pour éxécuter cette commande.");
			}
		}
		
	}

}
